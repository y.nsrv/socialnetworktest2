package itis.socialtest.entities;

import java.text.MessageFormat;

public class Author {

    private Long id;
    private String nickname;
    private String birthdayDate;

    public Author(Long id, String nickname, String birthdayDate) {
        this.id = id;
        this.nickname = nickname;
        this.birthdayDate = birthdayDate;
    }

    public Author() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getBirthdayDate() {
        return birthdayDate;
    }

    public void setBirthdayDate(String birthdayDate) {
        this.birthdayDate = birthdayDate;
    }

    @Override
    public String toString() {
        return MessageFormat.format(
                "Author'{'id={0}, nickname={1}, birthdayDate={2}}'",
                id, nickname, birthdayDate);
    }
}