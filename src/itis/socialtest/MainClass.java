package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.nio.charset.StandardCharsets.UTF_8;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в объекты в package "entities"
 * и осуществите над ними следующие операции:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private static List<Author> allAuthors;
    private List<Post> allPosts;
    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) {
        new MainClass().run("src/itis/socialtest/resources/PostDatabase.csv",
                "src/itis/socialtest/resources/Authors.csv");
    }

    private static Post readOneLinePost(String linePost) {
        String[] data = linePost.split(", ");

        Long authorId = Long.valueOf(data[0]);
        Author author = new Author();
        for (Author a : allAuthors) {
            if (a.getId().equals(authorId)) {
                author = a;
                break;
            }
        }
        Long likesCount = Long.valueOf(data[1]);
        String date = data[2];
        // В последней колонке в PostDatabase.csv в тексте содержатся символы,
        // которые совпадают с разделителями.
        String content = IntStream.range(3, data.length)
                .mapToObj(i -> data[i] + ", ")
                .collect(Collectors.joining());
        content = content.substring(0, content.length() - 2);

        return new Post(date, content, likesCount, author);
    }

    private static Author readOneLineAuthor(String lineAuthor) {
        String[] data = lineAuthor.split(", ");

        Long id = Long.valueOf(data[0]);
        String nickname = data[1];
        String birthdayDate = data[2];

        return new Author(id, nickname, birthdayDate);
    }

    private void run(String postsSourcePath, String authorsSourcePath) {
        try (BufferedReader brPosts = new BufferedReader(new InputStreamReader(
                new FileInputStream(new File(postsSourcePath)), UTF_8));
             BufferedReader brAuthors = new BufferedReader(new InputStreamReader(
                     new FileInputStream(new File(authorsSourcePath)), UTF_8))) {

            // Считываем авторов.
            allAuthors = brAuthors
                    .lines()
                    .map(MainClass::readOneLineAuthor)
                    .collect(Collectors.toList());
            // Считываем посты.
            allPosts = brPosts
                    .lines()
                    .map(MainClass::readOneLinePost)
                    .collect(Collectors.toList());

            System.out.println(allAuthors);
            System.out.println(allPosts);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        System.out.print("Все посты за сегодняшнюю дату: " +
                analyticsService.findPostsByDate(allPosts, "17.04.2021"));

        System.out.println("Все посты автора с ником \"varlamov\"" +
                analyticsService.findAllPostsByAuthorNickname(allPosts, "varlamov"));

        System.out.print("Cодержит ли текст хотя бы одного поста слово \"Россия\": " +
                analyticsService.checkPostsThatContainsSearchString(allPosts, "Россия"));
    }
}
